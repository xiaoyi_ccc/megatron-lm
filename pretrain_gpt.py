# Copyright (c) 2023, NVIDIA CORPORATION.  All rights reserved.
"""Pretrain GPT."""

import os
import torch
from functools import partial
from typing import Union
from megatron import get_args
from megatron import print_rank_0
from megatron import get_timers
from megatron import get_tokenizer
from megatron.core import mpu
from megatron.core.enums import ModelType
from megatron.core.datasets.blended_megatron_dataset_builder import BlendedMegatronDatasetBuilder
from megatron.core.datasets.gpt_dataset import GPTDatasetConfig
from megatron.core.datasets.gpt_dataset import MockGPTDataset, GPTDataset
import megatron.model
from megatron.core.models.gpt import GPTModel
from megatron.training import pretrain
from megatron.core.transformer.spec_utils import import_module
from megatron.utils import (
    get_batch_on_this_cp_rank,
    get_batch_on_this_tp_rank,
    average_losses_across_data_parallel_group
)
from megatron.arguments import core_transformer_config_from_args
from megatron.core.models.gpt.gpt_layer_specs import get_gpt_layer_with_transformer_engine_spec


def model_provider(pre_process=True, post_process=True) -> Union[GPTModel, megatron.model.GPTModel]:
    """Builds the model.

    If you set the use_mcore_models to True, it will return the mcore GPT model and if not the legacy GPT model.

    Args:
        pre_process (bool, optional): Set to true if you need to compute embedings. Defaults to True.
        post_process (bool, optional): Set to true if you need to want to compute output logits/loss. Defaults to True.


    Returns:
        Union[GPTModel, megatron.model.GPTModel]: The returned model
    """

    args = get_args()

    print_rank_0('building GPT model ...')
    # 在主节点打印信息
    config = core_transformer_config_from_args(get_args())

    if args.use_mcore_models:
        if args.spec is not None:
            transformer_layer_spec = import_module(args.spec)
        else:
            transformer_layer_spec = get_gpt_layer_with_transformer_engine_spec(args.num_experts, args.moe_grouped_gemm)

        model = GPTModel(
            config=config,
            transformer_layer_spec=transformer_layer_spec,
            vocab_size=args.padded_vocab_size,
            max_sequence_length=args.max_position_embeddings,
            pre_process=pre_process,
            post_process=post_process,
            fp16_lm_cross_entropy=args.fp16_lm_cross_entropy,
            parallel_output=True,
            share_embeddings_and_output_weights=not args.untie_embeddings_and_output_weights,
            position_embedding_type=args.position_embedding_type,
            rotary_percent=args.rotary_percent,
        )
    else:
        assert(args.context_parallel_size == 1), "Context parallelism is only supported with Megatron Core!"

        model = megatron.model.GPTModel(
            config,
            num_tokentypes=0,
            parallel_output=True,
            pre_process=pre_process,
            post_process=post_process
        )

    return model
    # 返回一个满足megatron并行化策略的模型实例具体实现在megatron/models/transformer.py中
    # 当前使用的gpt_model实现在Megatron-lm/core/models/gpt/gpt_model.py中


def get_batch(data_iterator):
    """Generate a batch."""

    # TODO: this is pretty hacky, find a better way

    # get_batch()方法从data_iterator中获取下一个batch
    # mpu代表并行状态
    if (not mpu.is_pipeline_first_stage()) and (not mpu.is_pipeline_last_stage()):
        return None, None, None, None, None
    # 如果当前的阶段不是流水线的第一个阶段并且也不是流水线的最后一个阶段就不需要传入batch让其训练

    # get batches based on the TP rank you are on
    # 根据tp_rank中的序号来获取相应的批次
    batch = get_batch_on_this_tp_rank(data_iterator)

    # slice batch along sequence dimension for context parallelism
    # 用于context并行的序列维度切片批处理，如果没有使用context并行则改行代码无用
    batch = get_batch_on_this_cp_rank(batch)

    return batch.values()

def loss_func(loss_mask: torch.Tensor, output_tensor: torch.Tensor):
    """Loss function.

    Args:
        loss_mask (torch.Tensor): Used to mask out some portions of the loss
        output_tensor (torch.Tensor): The tensor with the losses
    """
    args = get_args()

    losses = output_tensor.float()
    loss_mask = loss_mask.view(-1).float()
    # 将loss_mask展平为一维，并转换为float类型
    if args.context_parallel_size > 1:
        loss = torch.cat([torch.sum(losses.view(-1) * loss_mask).view(1), loss_mask.sum().view(1)])
        torch.distributed.all_reduce(loss, group=mpu.get_context_parallel_group())
        loss = loss[0] / loss[1]
    else:
        loss = torch.sum(losses.view(-1) * loss_mask) / loss_mask.sum()
    # 如果使用了context并行则需进行额外操作，否则将losses展平为一维并与loss_mask相乘，loss_mask中的0会使得相应的损失不被计算
    # 将损失值想加得到总损失，然后除以loss_mask中的非零元素数量，以计算平均损失

    # Check individual rank losses are not NaN prior to DP all-reduce.
    if args.check_for_nan_in_loss_and_grad:
        global_rank = torch.distributed.get_rank()
        assert not loss.isnan(), (
            f'Rank {global_rank}: found NaN in local forward loss calculation. '
            f'Device: {torch.cuda.current_device()}, node: {os.uname()[1]}'
        )

    # Reduce loss for logging.
    averaged_loss = average_losses_across_data_parallel_group([loss])
    # 在数据并行设置中，对不同设备上计算的损失进行all-reduce操作，得到全局平均损失

    return loss * args.context_parallel_size, {'lm loss': averaged_loss[0]}
    # 其中该函数接收经过损失函数处理后的结果，计算全局损失。函数返回一个loss标量和一个字典，其中字典包含的是训练中希望监听到的信息，此处为lm_loss也就是全局平均损失


def forward_step(data_iterator, model: GPTModel):
    """Forward training step.

    Args:
        data_iterator : Input data iterator
        model (GPTModel): The GPT Model
    """
    args = get_args()
    timers = get_timers()
    # 获取计时器实例，用于跟踪不同操作的时间

    # Get the batch.
    timers('batch-generator', log_level=2).start()
    # 记录batch-generator操作开始的时间
    tokens, labels, loss_mask, attention_mask, position_ids = get_batch(
        data_iterator)
    # 获取batch操作
    timers('batch-generator').stop()
    # 记录batch-generator操作结束的时间


    output_tensor = model(tokens, position_ids, attention_mask,
                          labels=labels)
    # labels 不为 None 时，会直接返回 loss (b, s)，否则返回 loss (b, s, h)

    return output_tensor, partial(loss_func, loss_mask)
    # 该函数定义了前向传播的过程，包括获取batch、前向传播、计算损失。


def is_dataset_built_on_rank():
    return (mpu.is_pipeline_first_stage() or mpu.is_pipeline_last_stage()) and mpu.get_tensor_model_parallel_rank() == 0
    # 如果当前正处于流水线并行的第一个stage或者最后一个stage，并且当前的张量并行组是流水线stage的第一个张量并行组则返回true

def core_gpt_dataset_config_from_args(args):
    tokenizer = get_tokenizer()

    return GPTDatasetConfig(
        is_built_on_rank=is_dataset_built_on_rank,
        random_seed=args.seed,
        sequence_length=args.seq_length,
        blend=args.data_path,
        blend_per_split=[args.train_data_path, args.valid_data_path, args.test_data_path],
        split=args.split,
        path_to_cache=args.data_cache_path,
        mock=args.mock_data,
        tokenizer=tokenizer,
        reset_position_ids=args.reset_position_ids,
        reset_attention_mask=args.reset_attention_mask,
        eod_mask_loss=args.eod_mask_loss,
        vocab_size=get_tokenizer().vocab_size,
    )
    # 获取gpt数据集的相关配置参数

def train_valid_test_datasets_provider(train_val_test_num_samples):
    """Build the train test and validation datasets.

    Args:
        train_val_test_num_samples : A list containing the number of samples in train test and validation.
    """
    args = get_args()

    config = core_gpt_dataset_config_from_args(args)

    if config.mock:
        dataset_type = MockGPTDataset
    else:
        dataset_type = GPTDataset
    # 根据配置参数判断当前数据集的种类是模拟gpt数据集还是真实数据集

    print_rank_0("> building train, validation, and test datasets for GPT ...")
    # 在主进程节点输出信息

    train_ds, valid_ds, test_ds = BlendedMegatronDatasetBuilder(
        dataset_type,
        train_val_test_num_samples,
        config
    ).build()
    # 构建数据集
    print_rank_0("> finished creating GPT datasets ...")
    # 在主进程节点输出信息

    return train_ds, valid_ds, test_ds
    # 构建训练、验证以及测试数据集


if __name__ == "__main__":

    # Temporary for transition to core datasets
    train_valid_test_datasets_provider.is_distributed = True
    # 将train_valid_test_datasets_provider的新属性is_distributed设置为true，标志当前处于分布式状态


    pretrain(train_valid_test_datasets_provider,
             model_provider,
             ModelType.encoder_or_decoder,
             forward_step,
             args_defaults={'tokenizer_type': 'GPT2BPETokenizer'})
    # 调用Megatron中的training.py的pretrain开始预训练
