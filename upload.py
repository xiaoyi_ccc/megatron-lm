import paramiko

def download_file_from_server(server_address, username, password, remote_file_path, local_file_path):
    try:
        # 创建SSH客户端对象
        ssh_client = paramiko.SSHClient()
        
        # 设置客户端策略以允许连接未知的主机
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        
        # 连接到服务器
        ssh_client.connect(hostname=server_address, username=username, password=password)
        
        # 创建SFTP客户端对象
        sftp_client = ssh_client.open_sftp()
        
        # 从服务器下载文件到本地
        sftp_client.get(remote_file_path, local_file_path)
        
        # 关闭SFTP客户端对象
        sftp_client.close()
        
        # 关闭SSH客户端对象
        ssh_client.close()
        
        print(f"文件已成功从服务器下载到本地：{local_file_path}")
    except Exception as e:
        print(f"下载文件时出现错误：{str(e)}")

# 示例用法
server_address = '服务器地址'
username = '用户名'
password = '密码'
remote_file_path = '服务器上的文件路径'
local_file_path = '本地文件保存路径'

download_file_from_server(server_address, username, password, remote_file_path, local_file_path)
